<?php

/*
 * Template Name: Welcome Page
 */

?>
<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
<meta charset="<?php bloginfo('charset');?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php // icons & favicons
$favicon_img = get_field('favicon_site', option);

if (!empty($favicon_img)): ?>

	<link rel="apple-touch-icon" href="<?php echo $favicon_img['url']; ?>">
	<link rel="icon" href="<?php echo $favicon_img['url']; ?>">
	<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo $favicon_img['url']; ?>">
	<![endif]-->

<?php endif;?>



<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head();?>
</head>

<body <?php body_class();?> class="coming-soon-page">

<div id="page" class="site">

	<div id="content" class="site-content">




 <?php $background_image_first_col = get_field('background_image_first_col');?>
 <div class="coming-soon-wrapper">
    <div class="coming-soon-img" style="background-image:url(<?php echo $background_image_first_col['url']; ?>)">
        <?php $logo_header = get_field('logo_header', 'option');?>
        <?php if ($logo_header) {?>
            <img src="<?php echo $logo_header['url']; ?>" alt="<?php echo $logo_header['alt']; ?>" />
        <?php }?>
    </div>
    <div class="coming-soon-text">

    <h1><?php the_field('heading');?></h1>

    <button id="showModal"><?php the_field('cta_text');?></button>

    </div>
 </div>



<div class="skh-modal__overlay">
  <div class="skh-modal  skh-effect-1">
    <div class="skh-modal__content">
      <!--Header-->
      <div class="modal__header">
        <span class="closeIcon"></span>
      </div>
      <!--Body-->
      <div class="modal__body subscribe-form-wrapper">

      <div class="subscribe-form-container">
        <div class="header">
        <h3><?php the_field('form_title');?></h3>
        </div>

        <div class="subscribe-form">
          <h6><?php the_field('subscribe_title');?></h6>

          <form action="<?php bloginfo('url'); ?>/thank-you" method="post">
            <div class="subscribe-btn">
              <input type="text" id="email" name="email" placeholder="Enter your email address">
              <button type="submit" name="submit" value="Submit"><?php the_field('subscribe_cta_text');?></button>
            </div>
          </form>
          
        </div>

      </div>
        
</div>
       
   

    </div>
  </div>
</div>

<?php get_footer();
