<?php

/*
 * Template Name:Thank You Page
 */

session_start();
$errors = '';
$your_email = '';
$email = '';

if (isset($_POST['submit'])) {
    $email = $_POST['email'];
    $landingpage = $_POST['redirect'];


    if (empty($errors)) {
        //send the email
        // $to = 'shekh.zyan@gmail.com';
        $to = 'lisa@wpropertygroup.co.uk';
        $subject = "Subscribed";
        $from = $email;
        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : '';

        $body = "From Website :<br/><br/>" .
            "Email : $email<br/><br/>" .
            "IP: $ip<br/>";

        $headers = 'MIME-Version: 1.0' . "\r\n";
        $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
        $headers .= "From: $from \r\n";
        $headers .= "Reply-To: $email \r\n";

        //mail($to, $subject, $body, $headers);

        if (mail($to, $subject, $body, $headers)) {
            // echo $to;
            // echo $subject;
            // echo $body;
            // echo $headers;
            header( "refresh:5;url=http://wpropertygroup.co.uk" ); 
        }

        //header('Location: ' . $landingpage);
    }
}


?>

<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
<meta charset="<?php bloginfo('charset');?>">
<meta name="viewport" content="width=device-width, initial-scale=1">

<?php // icons & favicons
$favicon_img = get_field('favicon_site', option);

if (!empty($favicon_img)): ?>

	<link rel="apple-touch-icon" href="<?php echo $favicon_img['url']; ?>">
	<link rel="icon" href="<?php echo $favicon_img['url']; ?>">
	<!--[if IE]>
		<link rel="shortcut icon" href="<?php echo $favicon_img['url']; ?>">
	<![endif]-->

<?php endif;?>



<link rel="profile" href="http://gmpg.org/xfn/11">

<?php wp_head();?>
</head>

<body <?php body_class();?> class="thanku-page">

<div id="page" class="site">

    <div id="content" class="site-content">
    
  <div class="container-fluid">
	<div class="row">
	  <div class="col-sm-12" style="padding:0 0px;">
	  			<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">
        <?php
			while ( have_posts() ) : the_post();

            the_content();

			endwhile; // End of the loop.
			?>

		</main><!-- #main -->
	</div><!-- #primary -->
	  </div>



	</div>
  </div>



<?php get_footer();